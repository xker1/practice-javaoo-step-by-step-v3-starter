package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private List<Klass> klasses;

    public Teacher(int id, String name, int age) {
        super(id, name, age);
        klasses = new ArrayList<Klass>();
    }

    public Teacher(int id, String name, int age, List<Klass> klasses) {
        super(id, name, age);
        this.klasses = klasses;
    }

    @Override
    public String introduce() {
        StringBuilder introduce = new StringBuilder(super.introduce());
        introduce.append(" I am a teacher.").append(" I teach Class ");
        for (Klass klass : klasses) {
            introduce.append(klass.getNumber());
            if (!klass.equals(klasses.get(klasses.size() - 1)))
                introduce.append(", ");
            else
                introduce.append(".");
        }
        return introduce.toString();
    }

    public void assignTo(Klass klass){
        klasses.add(klass);
    }

    public boolean belongsTo(Klass klass){
        if (this.klasses.isEmpty())
            return false;
        else if (klasses.contains(klass))
            return true;
        else
            return false;
    }

    public List<Klass> getKlasses() {
        return klasses;
    }

    public void setKlasses(List<Klass> klasses) {
        this.klasses = klasses;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
